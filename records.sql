USE firma_db;

LOCK TABLES `kunde` WRITE;
INSERT INTO `kunde` VALUES (1,'kunde_1'),(2,'kunde_2'),(3,'kunde_3');
UNLOCK TABLES;

LOCK TABLES `servicetechniker` WRITE;
INSERT INTO `servicetechniker` VALUES (1,'name_1'),(2,'name_2'),(3,'name_3');
UNLOCK TABLES;

LOCK TABLES `maschinen` WRITE;
INSERT INTO `maschinen` VALUES (1,40,'gut',1,1,'maschinen_1'),(2,23,'gut',1,2,'maschinen_2'),(3,10,'schlechter Zustand',2,3,'maschinen_3'),(4,100,'gut',3,3,'maschinen_4'),(5,50,'schlecht',3,1,'maschinen_5'),(6,200,'schlechter Zustand',2,2,'maschinen_6'),(7,50,'schlecht',1,3,'maschinen_7'),(8,35,'gut',1,1,'maschinen_8');
UNLOCK TABLES;

LOCK TABLES `ersatzteile` WRITE;
INSERT INTO `ersatzteile` VALUES (1,1,10,40,'name_1'),(2,2,2,2,'name_2'),(3,1,21,0,'name_3'),(4,5,40,20,'name_4'),(5,6,100,34,'name_5'),(6,3,50,10,'name_6'),(7,4,70,3,'name_7');
UNLOCK TABLES;