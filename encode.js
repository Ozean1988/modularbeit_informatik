/*
    Beispiel für eine gültige Eingabe: 
    FLOR = U+0046 U+004C U+004F U+0052
    >> FLOR => 01000110 01001100 01001111 01010010
*/
function encode_my_name(){
    console.log("Use encode method!");
    var inputText = document.getElementById("txtName").value;
    console.log(inputText);
    var txtNameEncoded = document.getElementById("txtNameEncoded");
    var encode ="";
    if(inputText.length>0){
        //Teilen Sie den Eingabetext
        if(inputText.includes("U+")){
            try{
                letters =  inputText.replaceAll("U+", "").split(" ");
                console.log(letters);
                letters.forEach(letter => {
                    binaty_value = parseInt(letter, 16).toString(2).padStart(8, '0');
                    encode = encode + binaty_value + " ";
                })
                txtNameEncoded.value=encode;

            }catch(err){
                inputText.value="";
                alert("Ungültige Eingabe");
            
            }
        }else{
            inputText.value="";
            alert("Ungültige Eingabe");
        }
    }
    else{
         inputText.value="";
         alert("Keine Eingabe");
    }
}

function reset_values(){
    var inputText = document.getElementById("txtName");
    var txtNameEncoded = document.getElementById("txtNameEncoded");
    inputText.value="";
    txtNameEncoded.value="";
}

