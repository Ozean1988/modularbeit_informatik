CREATE DATABASE  IF NOT EXISTS `firma_db`;
USE `firma_db`;

DROP TABLE IF EXISTS `kunde`;

CREATE TABLE `kunde` (
  `id` int NOT NULL AUTO_INCREMENT,
  `kunde` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

DROP TABLE IF EXISTS `servicetechniker`;

CREATE TABLE `servicetechniker` (
  `id` int NOT NULL AUTO_INCREMENT,
  `name` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

DROP TABLE IF EXISTS `maschinen`;

CREATE TABLE `maschinen` (
  `id` int NOT NULL AUTO_INCREMENT,
  `anzahl_der_einheiten` int DEFAULT NULL,
  `bedingung` varchar(45) DEFAULT NULL,
  `kunde_id` int DEFAULT NULL,
  `servicetechniker_id` int DEFAULT NULL,
  `name` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `kunde_id_idx` (`kunde_id`),
  KEY `servicetechniker_id_idx` (`servicetechniker_id`),
  CONSTRAINT `kunde_id` FOREIGN KEY (`kunde_id`) REFERENCES `kunde` (`id`),
  CONSTRAINT `servicetechniker_id` FOREIGN KEY (`servicetechniker_id`) REFERENCES `servicetechniker` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;


DROP TABLE IF EXISTS `ersatzteile`;

CREATE TABLE `ersatzteile` (
  `id` int NOT NULL AUTO_INCREMENT,
  `maschinen_id` int DEFAULT NULL,
  `preise` int DEFAULT NULL,
  `vorhanden` int DEFAULT NULL,
  `name` varchar(45) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `maschinen_id_idx` (`maschinen_id`),
  CONSTRAINT `maschinen_id` FOREIGN KEY (`maschinen_id`) REFERENCES `maschinen` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;



